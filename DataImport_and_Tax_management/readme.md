# Some additional Tips
1. [Some example of data import](#some-example-of-data-import)
   1. [Importing not demultiplexed data](#importing-not-demultiplexed-data)
   2. [Importing demultiplexed by using the manifest file](#importing-demultiplexed-by-using-the-manifest-file)
2. [Evaluating data quality](#evaluating-data-quality)
3. [Improving the Denoising step](#improving-the-denoising-step)
4. [Collections for taxonomic classification](#collections-for-taxonomic-classification)
5. [Export ASV table to tsv](#export-asv-table-to-tsv)
6. [Filter table to only have those samples in the metadata file](#filter-table-to-only-have-those-samples-in-the-metadata-file)
7. [Import ASV table obtaiend outside QIIME2 into it](#import-asv-table-obtaiend-outside-qiime2-into-it)  

---
***Always remember to active QIIME2 environment!!!***

```
source activate qiime2-2021.8
```
---

## Some example of data import
### Importing not demultiplexed data
During our tutorial we've faced with already demuliplexed data. Sometimes you need to perform the data demultiplexing, so let's demultiplex them!!!    
*** Check you are in your home folder***  
Let's create a folder for multiplexed data  in your **home folder**:
```
mkdir multiplexed_data && cd multiplexed_data
```  
Now we need to make a folder that will contain all our data:
```
mkdir emp-paired-end-sequences
```  
Let's download the required files:
```
wget \
  -O "emp-paired-end-sequences/forward.fastq.gz" \
  "https://data.qiime2.org/2020.2/tutorials/atacama-soils/10p/forward.fastq.gz"

wget \
  -O "emp-paired-end-sequences/reverse.fastq.gz" \
  "https://data.qiime2.org/2020.2/tutorials/atacama-soils/10p/reverse.fastq.gz"

wget \
  -O "emp-paired-end-sequences/barcodes.fastq.gz" \
  "https://data.qiime2.org/2020.2/tutorials/atacama-soils/10p/barcodes.fastq.gz"
```  
We are also going to reuse the ``sample-metadata.tsv`` file used during the QIIME2 tutorial:  
```
cp ~/qiime2-atacama-tutorial/sample-metadata.tsv .
```
Now let's import everything as a QIIME2 artifact:  
```
qiime tools import \
   --type EMPPairedEndSequences \
   --input-path emp-paired-end-sequences \
   --output-path emp-paired-end-sequences.qza
```
So we can perform the demultiplexing step:  
```
qiime demux emp-paired \
  --m-barcodes-file sample-metadata.tsv \
  --m-barcodes-column barcode-sequence \
  --p-rev-comp-mapping-barcodes \
  --i-seqs emp-paired-end-sequences.qza \
  --o-per-sample-sequences demux.qza \
  --o-error-correction-details demux-details.qza
```

Let's visualize our data!!!  
```
qiime demux summarize \
  --i-data demux.qza \
  --o-visualization demux.qzv
```

### Importing demultiplexed by using the manifest file
During the mail tutorial we imported the PE fastq files by using:  
 - **type** `SampleData[PairedEndSequencesWithQuality]`: which means we are using PE fastq files;
 - **input-format** `CasavaOneEightSingleLanePerSampleDirFmt`: which means the file name are formatted by using the **CASAVA** file format.  

Sometimes our data are not formatted according to **CASAVA** format so we need to use an alternative way to import the data.  
To import the data we need to generate a **manifest file**.  

Initially we need to create a new folder in our home just because we are going to re-use this data for other stuff.  
```
cd

mkdir IJMS_training_test && cd IJMS_training_test
```    
Let's have a look to our data:
```
ls ~/Share/IJMS_data/IJMS_input_data
```

Now we can create our file named `manifest_file.tsv`:

```
echo sample-id,forward-absolute-filepath,reverse-absolute-filepath > manifest_file.tsv
echo 211446F203610,$HOME/Share/IJMS_data/IJMS_input_data/211446F203610_S155_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/211446F203610_S155_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 211454F203618,$HOME/Share/IJMS_data/IJMS_input_data/211454F203618_S163_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/211454F203618_S163_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 211456F203620,$HOME/Share/IJMS_data/IJMS_input_data/211456F203620_S165_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/211456F203620_S165_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 211460F203624,$HOME/Share/IJMS_data/IJMS_input_data/211460F203624_S169_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/211460F203624_S169_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 214981F203626,$HOME/Share/IJMS_data/IJMS_input_data/214981F203626_S2_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/214981F203626_S2_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 214991F203636,$HOME/Share/IJMS_data/IJMS_input_data/214991F203636_S12_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/214991F203636_S12_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 214993F203638,$HOME/Share/IJMS_data/IJMS_input_data/214993F203638_S14_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/214993F203638_S14_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 214997F203642,$HOME/Share/IJMS_data/IJMS_input_data/214997F203642_S18_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/214997F203642_S18_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 215001F203646,$HOME/Share/IJMS_data/IJMS_input_data/215001F203646_S22_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/215001F203646_S22_L001_R2_001.fastq.gz >> manifest_file.tsv
echo 215003F203648,$HOME/Share/IJMS_data/IJMS_input_data/215003F203648_S24_L001_R1_001.fastq.gz,$HOME/Share/IJMS_data/IJMS_input_data/215003F203648_S24_L001_R2_001.fastq.gz >> manifest_file.tsv
```
A little trick for our manifest file.  
```
sed -i -e 's/,/\t/g' manifest_file.tsv 
```
Now we're ready to import our data and generate a visualization file:  
```
qiime tools import \
  --type 'SampleData[PairedEndSequencesWithQuality]' \
  --input-path manifest_file.tsv \
  --output-path pe-demux.qza \
  --input-format PairedEndFastqManifestPhred33V2
  
qiime demux summarize \
    --i-data  pe-demux.qza \
    --o-visualization pe-demux.qzv
```

## Evaluating data quality
We have already learned to use **FastQC**. It is a really useful tool but the main drowback is it generates a report for file for each analysed fastq file.  
So it is not so simple to figure out what is overall the quality of our raw data.  
A solution is to apply [**MultiQC**](https://multiqc.info/) a tool allowing to *aggregate results from bioinformatics analyses across many samples into a single report*.  

Create a folder that will contain all the FastQC reports:  
```
cd

mkdir ~/fastqc_reports 
```
Execute FastQC on our raw data. In order to save time we're going to evaluate only `Baquedano`. In our test case, it is simple cause those file names start with `BAQ`.  
   
```
fastqc  ~/Share/raw_data_tutorial/BAQ2420* -O fastqc_reports
```
    
```
multiqc -n sequencing_data_report fastqc_reports/
```

Now by using *FileZilla* download on your computers these items:  
* `sequencing_data_report.html`  
* `sequencing_data_report_data`  

## Improving the Denoising step
Now let's try to improve our denoising step by modifying **DADA2** import parameters:  
```
cd ~/qiime2-atacama-tutorial
mkdir denoising_alt && cd denoising_alt
```

Now we re-perform the denoising step by modifying the **Expected Error (ee)** threshold.  
Just remember **ee** is a way to measure the number of nucleotides that are probably wrong in our sequences:  
![](ee-2.png)  

The option we introduce are `--p-max-ee-f` and `--p-max-ee-f`.
```
qiime dada2 denoise-paired \
  --i-demultiplexed-seqs ../demux-paired-end.qza \
  --p-trim-left-f 13 \
  --p-trim-left-r 13 \
  --p-trunc-len-f 150 \
  --p-trunc-len-r 150 \
  --o-table table_ee.qza \
  --p-max-ee-f 3 \
  --p-max-ee-r 3 \
  --o-representative-sequences rep-seqs_ee.qza \
  --o-denoising-stats denoising-stats_ee.qza
```

```
qiime metadata tabulate --m-input-file denoising-stats_ee.qza --o-visualization denoising-stats_ee.qzv
```

Now try to compare this results with the one obtained during the QIIME2 tutorial.    

## Collections for taxonomic classification
In order to perform taxonomic classification in QIIME2 we need to properly import and, eventually, train reference collections:
The available in databases [**QIIME2 Data resoruces**](https://docs.qiime2.org/2020.8/data-resources/) are:  
* [SILVA](https://www.arb-silva.de/)
* [GreenGenes](https://www.nature.com/articles/ismej2011139)  
* [UNITE](https://unite.ut.ee/)  

If your are interested in metabarcoding analysis based on the **COX1** barcode, you may consider [**MIDORI**](http://www.reference-midori.info/).  
 
The easiest case is *SILVA*. QIIME Developers have already created an appropriate version so you can just dowload it:  
```
cd
mkdir tax_import && cd tax_import
```
So let's start with `qza` data download:  
```
wget --no-check-certificate https://data.qiime2.org/2021.8/common/silva-138-99-seqs.qza
wget --no-check-certificate https://data.qiime2.org/2021.8/common/silva-138-99-tax.qza
```
If you plan to use alignment based approaches for taxonomic assigment (i.e. BLAST or VSEARCH), that's enough. You may proceed with ASV classification.  
Otherwise, if you would like to use the **sklearn** approach you need to train the classifier.  
For 16S data, it is recommended to firstly extract the region you're interested in by using your primer sequences:  
In the **EXAMPLE** below we are extracting the V3-V4 hyper-variable regions from SILVA 16S full-lenght sequences by using Illumina V3V4 primers.   

```
qiime feature-classifier extract-reads \
  --i-sequences silva-138-99-seqs.qza \
  --p-f-primer CCTACGGGNGGCWGCAG \
  --p-r-primer GGACTACNVGGGTWTCTAAT \
  --o-reads v3v4.ref-seqs.qza
```

There are some additional options you may consider:
 * `--p-min-length`: min amplicon length  
 * `--p-max-length`: max amplicon length  

And finally, let's train the classifier:
```
qiime feature-classifier fit-classifier-naive-bayes  \
    --i-reference-reads v3v4.ref-seqs.qza \
    --i-reference-taxonomy silva-138-99-tax.qza \
    --o-classifier vv34.SILVA_138_NR_99_classifier.qza
```
In some cases, you also need to import reference sequences and taxonomy in QIIME2.
Let's try to import MIDORI data in QIIME2:
```
cd

mkdir MIDORI_ref

cd MIDORI_ref 
```
Download MIDORI data. It has been already properly formatted for QIIME2 import:
```
wget --no-check-certificate http://www.reference-midori.info/download/Latest_GenBankRelease245/QIIME/uniq/MIDORI_UNIQ_NUC_GB245_CO1_QIIME.fasta.gz;
wget --no-check-certificate http://www.reference-midori.info/download/Latest_GenBankRelease245/QIIME/uniq/MIDORI_UNIQ_NUC_GB245_CO1_QIIME.taxon.gz;
```
```
unzip MIDORI_UNIQ_NUC_GB245_CO1_QIIME.fasta.gz 
unzip MIDORI_UNIQ_NUC_GB245_CO1_QIIME.taxon.gz
```

```
qiime tools import \
  --input-path MIDORI_UNIQ_NUC_GB245_CO1_QIIME.fasta \
  --output-path MIDORI_UNIQ_NUC_GB245_CO1_QIIME.qza \
  --type 'FeatureData[Sequence]'
  
qiime tools import \
 --type FeatureData[Taxonomy] \
 --input-path MIDORI_UNIQ_NUC_GB245_CO1_QIIME.taxon \
 --input-format HeaderlessTSVTaxonomyFormat \
 --output-path MIDORI_UNIQ_NUC_GB245_CO1_QIIME.taxon.qza
```

## Export ASV table to tsv
Sometimes you need to export you ASV table to TSV:
```
cd ~/qiime2-atacama-tutorial

qiime tools export --input-path table_16S.qza --output-path ASV_table_exports/
```
We obtain the ASV table in [BIOM]() format. Let's convert it in tsv.
```
biom convert -i ASV_table_exports/feature-table.biom -o ASV_table_exports/feature-table.txt --to-tsv
```
Note that this table doesn't currently contain taxonomy information. To merge the tables you can use the ```qiime taxa collapse``` command below at ASV level, or the function in the taxonomy barcharts visualisation.

## Filter table to only have those samples in the metadata file
Filtering only certain samples or features. Useful to remove anomalies, negative/positive controls or failed samples, or subset data for specific analysis
```
cd ~/qiime2-atacama-tutorial
```

Let's create ad small metadata file containing only 8 files. 
```
head -n 10 sample-metadata.tsv > my_metadata.csv
```

```
qiime feature-table filter-samples \
  --i-table table_16S.qza \
  --m-metadata-file my_metadata.csv \
  --o-filtered-table my_table.qza
```

Filter table by a specific metadata column value: 
```
qiime feature-table filter-samples \
  --i-table table_16S.qza \
  --m-metadata-file sample-metadata.tsv \
  --p-where "[transect-name]='Baquedano'" \
  --o-filtered-table Baquedano.qza
```

Remove features with less than 100 reads in at least 4 samples
```
qiime feature-table filter-features \
  --i-table table_16S.qza \
  --p-min-frequency 100 \
  --p-min-samples 4 \
  --o-filtered-table table-selected.qza
```

Collapse the whole table into a taxonomy level (i.e. family) and include taxonomy description
```
qiime taxa collapse \
  --i-table table_16S.qza \
  --i-taxonomy taxonomy_16S_SKLEARN.qza \
  --p-level 4 \
  --o-collapsed-table table-l4.qza
```

## Import ASV table obtaiend outside QIIME2 into it
Sometimes it is useful to apply DADA2 outside QIIME2. Nonetheless is always possibile to import DADA2 produced ASV table in QIIME2.
```
cd

mkdir dada2_data_import && cd dada2_data_import

cp ~/Share/seqtab-nochim.txt .
```
Let's inspect how `seqtab-nochim.txt` is made:
```
less seqtab-nochim.txt
```
Actually the problem is we have features sequence instead of md5 name in the ASV column. We may convert it by using **Python**.  
Enter the Python Shell:
```
ipython
```
First of all we import the needed functions:  
```
import hashlib
import sys
import pandas as pd
```
Then we generate the file for ASV table and representative sequences and convert our table:
```
df = pd.read_csv("seqtab-nochim.txt", sep = "\t", index_col = 0, header = 0)
with open("rep-seqs_HEX.fna", "w") as tmp:
    new_asv = []
    for i in df.index:
      m = hashlib.md5()
      m.update(i.encode('utf-8'))
      tmp.write(">{}\n{}\n".format(m.hexdigest(), i))
      new_asv.append(m.hexdigest())
    df.index = new_asv
df.to_csv("seqtab-nochim_hash.txt", sep="\t",quoting=False)
```

Finally close Python;
```
quit()
```
Now we're ready to import data in QIIME2!!!
First convert the textual table into a biom table:
```
biom convert -i seqtab-nochim_hash.txt -o seqtab-nochim.biom --table-type="OTU table" --to-hdf5

qiime tools import \
--input-path seqtab-nochim.biom \
--type 'FeatureTable[Frequency]' \
--input-format BIOMV210Format \
--output-path feature-table.qza
```
Then import your sequences:
```
qiime tools import \
--input-path rep-seqs_HEX.fna \
--type 'FeatureData[Sequence]' \
--output-path rep-seqs.qza
```
Finally, we may inspect our feature table:
```
qiime feature-table summarize \
   --i-table feature-table.qza \
   --o-visualization feature-table.qzv 
```



[**Back to the program**](../README.md)